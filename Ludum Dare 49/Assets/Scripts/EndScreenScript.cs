using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;

public class EndScreenScript : MonoBehaviour
{
    [SerializeField] private GameObject endScreen;
    [SerializeField] private GameObject timeTrialText;
    [SerializeField] private GameObject enduranceText;

    [SerializeField] private TextMeshProUGUI triviaNumber;

    [SerializeField] private PlayerScript playerScript;

    [SerializeField] AudioSource audioSource;

    private void Awake()
    {
        endScreen.SetActive(false);
        audioSource = GetComponent<AudioSource>();
    }

    public void ShowEndScreen(string towerHeight, PlayMode playMode)
    {
        audioSource.Play();

        playerScript.controlsActive = false;
        playerScript.DisableControls();
        playerScript.PublicDetachSpring();

        if (playMode == PlayMode.TimeTrial)
        {
            timeTrialText.SetActive(true);
            enduranceText.SetActive(false);
        }
        else
        {
            enduranceText.SetActive(true);
            timeTrialText.SetActive(false);
        }

        endScreen.SetActive(true);

        triviaNumber.text = towerHeight;
    }

    public void RestartButtonPressed()
    {
        Time.timeScale = 1;
        SceneManager.LoadScene("GameScene");
    }

    public void MainMenuButtonPressed()
    {
        Time.timeScale = 1;
        SceneManager.LoadScene("MainMenu");
    }

    public void QuitButtonPressed()
    {
        if (Application.platform != RuntimePlatform.WebGLPlayer)
        {
            Application.Quit();
        }
    }
}
