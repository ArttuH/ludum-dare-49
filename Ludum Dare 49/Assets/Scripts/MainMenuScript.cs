using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenuScript : MonoBehaviour
{
    [SerializeField] GameObject playModePrefab;

    [SerializeField] private GameObject buttons;
    [SerializeField] private GameObject optionsView;
    [SerializeField] private GameObject creditsView;
    [SerializeField] private GameObject playModesView;

    private void Awake()
    {
        buttons.SetActive(true);
        optionsView.SetActive(false);
        creditsView.SetActive(false);
        playModesView.SetActive(false);
    }

    public void PlayButtonPressed()
    {
        buttons.SetActive(false);
        playModesView.SetActive(true);
    }

    public void OptionsButtonPressed()
    {
        buttons.SetActive(false);
        optionsView.SetActive(true);
    }

    public void CreditsButtonPressed()
    {
        buttons.SetActive(false);
        creditsView.SetActive(true);
    }

    public void BackFromOptionsButtonPressed()
    {
        optionsView.SetActive(false);
        buttons.SetActive(true);
    }

    public void BackFromCreditsButtonPressed()
    {
        creditsView.SetActive(false);
        buttons.SetActive(true);
    }

    public void TimedChallengeButtonPressed()
    {
        GameObject playModeObject = GameObject.Find("PlayModeObject(Clone)");
        if (playModeObject == null)
        {
            playModeObject = Instantiate(playModePrefab);
        }
        PlayModeScript playModeScript = playModeObject.GetComponent<PlayModeScript>();
        playModeScript.playMode = PlayMode.TimeTrial;
        SceneManager.LoadScene("GameScene");
    }

    public void NoDropChallengeButtonPressed()
    {
        GameObject playModeObject = GameObject.Find("PlayModeObject(Clone)");
        if (playModeObject == null)
        {
            playModeObject = Instantiate(playModePrefab);
        }
        PlayModeScript playModeScript = playModeObject.GetComponent<PlayModeScript>();
        playModeScript.playMode = PlayMode.Endurance;
        SceneManager.LoadScene("GameScene");
    }

    public void BackFromPlayModesButtonPressed()
    {
        playModesView.SetActive(false);
        buttons.SetActive(true);
    }

    public void QuitButtonPressed()
    {
        if (Application.platform != RuntimePlatform.WebGLPlayer)
        {
            Application.Quit();
        }
    }
}
