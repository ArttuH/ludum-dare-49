using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoxScript : MonoBehaviour
{
    [SerializeField] GameObject impactAudioPrefab;

    [SerializeField] Material spawnMaterial;
    [SerializeField] Material validMaterial;
    [SerializeField] Material invalidMaterial;

    public bool isValid;
    public bool isGrabbed;
    public bool doNotDrop;

    MeshRenderer meshRenderer;

    float validationDelay;
    float validationTimer;

    [SerializeField] float spawnMaterialDuration;
    [SerializeField] float spawnTimer;

    bool initialized;
    bool materialSpawnDone;

    [SerializeField] AudioSource validationAudioSource;
    [SerializeField] AudioSource attachAudioSource;

    [SerializeField] AudioClip attach;
    [SerializeField] AudioClip detach;

    private void Awake()
    {
        isValid = true;
        isGrabbed = false;
        initialized = false;
        materialSpawnDone = false;
        doNotDrop = false;
    }

    public void Initialize()
    {
        meshRenderer = GetComponent<MeshRenderer>();
        meshRenderer.material = spawnMaterial;

        validationDelay = 3f;
        spawnTimer = spawnMaterialDuration;

        initialized = true;
    }

    private void Update()
    {
        if (initialized)
        {
            if (!materialSpawnDone)
            {
                if (spawnTimer > 0f)
                {
                    spawnTimer -= Time.deltaTime;
                    meshRenderer.material.SetFloat("_Cutoff", Mathf.Lerp(-2f, 2f, 1.0f - spawnTimer / spawnMaterialDuration));
                }
                else
                {
                    materialSpawnDone = true;

                    if (isValid)
                    {
                        meshRenderer.sharedMaterial = validMaterial;
                    }
                    else
                    {
                        meshRenderer.sharedMaterial = invalidMaterial;
                    }
                }
            }

            if (!isGrabbed && !isValid)
            {
                validationTimer -= Time.deltaTime;
                if (validationTimer <= 0f)
                {
                    Validate();
                }
            }
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (!isValid && !isGrabbed && !collision.collider.gameObject.CompareTag("Player") && materialSpawnDone)
        {
            Validate();
        }

        if (materialSpawnDone)
        {
            float relativeVelocity = collision.relativeVelocity.magnitude;
            float impulse = collision.impulse.magnitude;

            GameObject impactAudio = Instantiate(impactAudioPrefab, transform.position, transform.rotation);
            ImpactAudioScript impactAudioScript = impactAudio.GetComponent<ImpactAudioScript>();
            impactAudioScript.SetProperties(collision, transform.localScale.x);
        }
    }

    public void StartGrab()
    {
        isGrabbed = true;
        Invalidate();
        attachAudioSource.PlayOneShot(attach);
    }

    public void EndGrab()
    {
        isGrabbed = false;
        attachAudioSource.PlayOneShot(detach);

        //Invoke("Validate", 3f);
    }

    void Validate()
    {
        isValid = true;
        meshRenderer.sharedMaterial = validMaterial;

        validationAudioSource.Play();
    }

    void Invalidate()
    {
        isValid = false;
        validationTimer = validationDelay;
        meshRenderer.sharedMaterial = invalidMaterial;
    }
}
