using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum PlayMode
{
    TimeTrial,
    Endurance
}

public class PlayModeScript : MonoBehaviour
{
    public PlayMode playMode;

    private void Awake()
    {
        DontDestroyOnLoad(gameObject);
    }
}
