using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ImpactAudioScript : MonoBehaviour
{
    public float volume;
    public float pitch;

    AudioSource audioSource;

    [SerializeField] AudioClip[] audioClips;

    [SerializeField] float minImpulse;
    [SerializeField] float maxImpulse;

    [SerializeField] float minVolume;
    [SerializeField] float maxVolume;

    private void Awake()
    {
        audioSource = GetComponent<AudioSource>();
    }

    // Start is called before the first frame update
    void Start()
    {
        audioSource.volume = volume;
        audioSource.pitch = pitch;

        int index = Random.Range(0, audioClips.Length);

        audioSource.PlayOneShot(audioClips[index]);

        Destroy(gameObject, 5f);
    }

    public void SetProperties(Collision collision, float scale)
    {
        float impulse = Mathf.Max(minImpulse, collision.impulse.magnitude);
        impulse = Mathf.Min(maxImpulse, impulse);

        volume = Mathf.Lerp(minVolume, maxVolume, (impulse - minImpulse) / (maxImpulse - minImpulse));
        pitch = 1 / scale;
    }
}
