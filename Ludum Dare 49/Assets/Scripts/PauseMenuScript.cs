using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.InputSystem;

public class PauseMenuScript : MonoBehaviour
{
    [SerializeField] private GameObject pauseMenu;

    [SerializeField] private bool isPaused;

    PlayerControls playerControls;

    [SerializeField] GameObject timeLeftParent;
    [SerializeField] GameObject towerHeightParent;

    [SerializeField] PlayerScript playerScript;

    PlayModeScript playModeScript;

    private void Awake()
    {
        pauseMenu.SetActive(false);
        isPaused = false;
        playerControls = new PlayerControls();
        playerControls.Menu.Pause.performed += TogglePause;

        GameObject playModeObject = GameObject.Find("PlayModeObject(Clone)");
        playModeScript = playModeObject.GetComponent<PlayModeScript>();
    }

    private void OnEnable()
    {
        playerControls.Enable();
    }

    private void OnDisable()
    {
        playerControls.Disable();
    }

    private void Update()
    {
        /*if (Input.GetKeyDown(KeyCode.Escape) || Input.GetKeyDown(KeyCode.Space))
        {
            if (!isPaused)
            {
                PauseButtonPressed();
            }
            else
            {
                ContinueButtonPressed();
            }
        }*/
    }

    void TogglePause(InputAction.CallbackContext callbackContext)
    {
        if (!isPaused)
        {
            PauseButtonPressed();
        }
        else
        {
            ContinueButtonPressed();
        }
    }

    public void PauseButtonPressed()
    {
        pauseMenu.SetActive(true);
        Time.timeScale = 0;
        isPaused = true;
        towerHeightParent.SetActive(false);
        timeLeftParent.SetActive(false);

        playerScript.DisableControls();
    }

    public void ContinueButtonPressed()
    {
        pauseMenu.SetActive(false);
        Time.timeScale = 1;
        isPaused = false;
        towerHeightParent.SetActive(true);

        if (playModeScript.playMode == PlayMode.TimeTrial)
        {
            timeLeftParent.SetActive(true);
        }

        playerScript.EnableControls();
    }

    public void RestartButtonPressed()
    {
        Time.timeScale = 1;
        SceneManager.LoadScene("GameScene");
    }

    public void MainMenuButtonPressed()
    {
        Time.timeScale = 1;
        SceneManager.LoadScene("MainMenu");
    }

    public void QuitButtonPressed()
    {
        if (Application.platform != RuntimePlatform.WebGLPlayer)
        {
            Application.Quit();
        }
    }
}
