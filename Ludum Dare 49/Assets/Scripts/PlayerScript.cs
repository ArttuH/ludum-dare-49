using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using Cinemachine;

public class PlayerScript : MonoBehaviour
{
    PlayerControls playerControls;

    public Vector2 moveInput;

    bool springAttached;

    [SerializeField] AudioSource springAudioSource;
    [SerializeField] AudioSource flyingAudioSource;

    [SerializeField] CinemachineVirtualCamera playerCamera;
    [SerializeField] CinemachineVirtualCamera boxCamera;
    [SerializeField] CinemachineTargetGroup boxCameraTargetGroup;

    [SerializeField] GameObject attachedObject;

    [SerializeField] float springLookMaxDistance;
    [SerializeField] float springAttachSpeed;
    [SerializeField] float springStrength;

    [SerializeField] float maxSpeed;
    [SerializeField] float friction;
    [SerializeField] float acceleration;

    [SerializeField] float springAudioMinLength;
    [SerializeField] float springAudioMaxLength;
    [SerializeField] float springAudioMinPitch;
    [SerializeField] float springAudioMaxPitch;

    [SerializeField] float flyingAudioMinVelocity;
    [SerializeField] float flyingAudioMaxVelocity;
    [SerializeField] float flyingAudioMinPitch;
    [SerializeField] float flyingAudioMaxPitch;
    [SerializeField] float flyingAudioMinVolume;
    [SerializeField] float flyingAudioMaxVolume;

    Rigidbody playerRigidbody;

    Vector3 springEnd;
    SpringJoint activeSpringJoint;

    LineRenderer springRenderer;
    bool springShouldRender;

    public bool controlsActive;

    private void Awake()
    {
        playerControls = new PlayerControls();
        playerControls.Gameplay.ToggleSpring.started += ToggleSpring;

        springAttached = false;
        springShouldRender = false;

        playerRigidbody = GetComponent<Rigidbody>();
        springRenderer = GetComponent<LineRenderer>();
        springAudioSource = GetComponent<AudioSource>();

        playerCamera.Priority = 1;
        boxCamera.Priority = 0;

        controlsActive = true;
    }

    private void OnEnable()
    {
        playerControls.Enable();
    }

    private void OnDisable()
    {
        playerControls.Disable();
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        ReadInputs();

        DrawSpring();
    }

    private void FixedUpdate()
    {
        //transform.Translate(moveInput * 0.1f);

        playerRigidbody.velocity += new Vector3(moveInput.x, moveInput.y) * acceleration * Time.deltaTime;
        if (playerRigidbody.velocity.magnitude > maxSpeed)
        {
            playerRigidbody.velocity = playerRigidbody.velocity.normalized * maxSpeed;
        }

        if (Mathf.Abs(moveInput.x) < 0.01f)
        {
            playerRigidbody.velocity -= new Vector3(playerRigidbody.velocity.x, 0, 0) * friction * Time.deltaTime;
        }

        if (Mathf.Abs(moveInput.y) < 0.01f)
        {
            playerRigidbody.velocity -= new Vector3(0, playerRigidbody.velocity.y, 0) * friction * Time.deltaTime;
        }

        UpdateFlyingAudio();
    }

    void ReadInputs()
    {
        if (controlsActive)
        {
            moveInput = playerControls.Gameplay.Move.ReadValue<Vector2>();
        }
        else
        {
            moveInput = Vector2.zero;
        }
    }

    public void EnableControls()
    {
        playerControls.Enable();
    }

    public void DisableControls()
    {
        playerControls.Disable();
    }

    void ToggleSpring(InputAction.CallbackContext callbackContext)
    {
        //Debug.Log("ToggleSpring()!");

        if (springAttached)
        {
            DetachSpring();
        }
        else
        {
            ActivateSpring();
        }
    }

    void AttachSpring(RaycastHit raycastHit)
    {
        //Debug.Log("AttachSpring()!");

        springAttached = true;

        GameObject box = raycastHit.collider.gameObject;
        Transform boxTransform = box.transform;

        box.AddComponent<SpringJoint>();
        SpringJoint springJoint = box.GetComponent<SpringJoint>();
        springJoint.connectedBody = playerRigidbody;

        Vector3 anchorPoint = boxTransform.InverseTransformPoint(raycastHit.point);

        springJoint.anchor = new Vector3(
            anchorPoint.x * boxTransform.localScale.x,
            anchorPoint.y * boxTransform.localScale.y);
        
        Debug.DrawLine(transform.position, raycastHit.point, Color.red, 10f);
        Debug.DrawLine(transform.position, boxTransform.position, Color.blue, 10f);
        Debug.DrawLine(boxTransform.position, boxTransform.position + springJoint.anchor, Color.green, 10f);

        springJoint.enableCollision = true;
        springJoint.autoConfigureConnectedAnchor = false;
        springJoint.connectedAnchor = new Vector3(0, springJoint.connectedAnchor.y, 0);
        springJoint.spring = springStrength;

        activeSpringJoint = springJoint;

        attachedObject = box;
        springEnd = raycastHit.point;

        BoxScript boxScript = box.GetComponent<BoxScript>();
        boxScript.StartGrab();

        playerCamera.Priority = 0;
        boxCamera.Priority = 1;
        boxCameraTargetGroup.AddMember(box.transform, 1.0f, box.transform.transform.localScale.magnitude * 2f);
    }

    public void PublicDetachSpring()
    {
        DetachSpring();
    }

    void DetachSpring()
    {
        if (attachedObject != null)
        {
            SpringJoint springJoint = attachedObject.GetComponent<SpringJoint>();

            if (springJoint != null)
            {
                Destroy(springJoint);
            }

            BoxScript boxScript = attachedObject.GetComponent<BoxScript>();
            boxScript.EndGrab();

            boxCameraTargetGroup.RemoveMember(attachedObject.transform);
            playerCamera.Priority = 1;
            boxCamera.Priority = 0;
            attachedObject = null;
        }

        springRenderer.enabled = false;
        springShouldRender = false;
        springAttached = false;

        springAudioSource.Stop();
    }

    void ActivateSpring()
    {
        //Debug.Log("Activating spring!");

        springShouldRender = true;
        springAudioSource.Play();

        RaycastHit raycastHit = DetectBox();

        //yield return new WaitForSeconds(1f);

        bool validHit = false;

        if (raycastHit.collider != null)
        {
            //Debug.Log($"Raycast hit {raycastHit.collider}!");

            if (raycastHit.collider.gameObject.CompareTag("Grab"))
            {
                validHit = true;
            }
            else
            {
                //Debug.Log("Raycast hit something that's not grabbable!");
            }
        }
        else
        {
            //Debug.Log("Raycast didn't hit anything!");
        }

        StartCoroutine(AnimateSpring(raycastHit, validHit));

        //Debug.Log("Spring is finished!");
    }

    IEnumerator AnimateSpring(RaycastHit raycastHit, bool validHit)
    {
        //Debug.Log($"validHit == {validHit.ToString()}");

        springShouldRender = true;

        Vector3 target = transform.position + Vector3.down * springLookMaxDistance;

        if (raycastHit.collider != null)
        {
            target = raycastHit.point;
        }

        float progress = 0f;
        float traveledDistance = 0f;

        springEnd = transform.position;

        float targetDistance = Vector3.Distance(target, transform.position);

        while (progress < 1f)
        {
            float step = springAttachSpeed * Time.deltaTime;
            springEnd = springEnd + (target - springEnd).normalized * step;

            traveledDistance += step;

            progress = traveledDistance / targetDistance;

            yield return new WaitForEndOfFrame();
        }

        springEnd = target;

        if (validHit)
        {
            AttachSpring(raycastHit);
        }
        else
        {
            springShouldRender = false;
            springRenderer.enabled = false;
            springAudioSource.Stop();
        }
    }

    RaycastHit DetectBox()
    {
        RaycastHit raycastHit;
        Physics.Raycast(transform.position, Vector3.down, out raycastHit, springLookMaxDistance);

        return raycastHit;
    }

    void DrawSpring()
    {
        if (springShouldRender)
        {
            if (!springRenderer.enabled)
            {
                springRenderer.enabled = true;
            }

            if (attachedObject != null && activeSpringJoint != null)
            {
                springEnd = attachedObject.transform.position +
                    (attachedObject.transform.right * activeSpringJoint.anchor.x +
                    attachedObject.transform.up * activeSpringJoint.anchor.y) * 0.99f;
            }

            springRenderer.SetPosition(0, transform.position);
            springRenderer.SetPosition(1, springEnd);

            float springLength = Vector3.Distance(transform.position, springEnd);

            springLength = Mathf.Max(springAudioMinLength, springLength);
            springLength = Mathf.Min(springAudioMaxLength, springLength);

            float pitchProgress = (springLength - springAudioMinLength) / (springAudioMaxLength - springAudioMinLength);

            springAudioSource.pitch = Mathf.Lerp(springAudioMinPitch, springAudioMaxPitch, pitchProgress);

            //Debug.DrawLine(transform.position, springEnd);
        }
    }

    void UpdateFlyingAudio()
    {
        float velocity = playerRigidbody.velocity.magnitude;

        velocity = Mathf.Max(flyingAudioMinVelocity, velocity);
        velocity = Mathf.Min(flyingAudioMaxVelocity, velocity);

        float velocityProgress = (velocity - flyingAudioMinVelocity) / (flyingAudioMaxVelocity - flyingAudioMinVelocity);

        flyingAudioSource.volume = Mathf.Lerp(flyingAudioMinVolume, flyingAudioMaxVolume, velocityProgress);
        flyingAudioSource.pitch  = Mathf.Lerp(flyingAudioMinPitch, flyingAudioMaxPitch, velocityProgress);
    }
}
