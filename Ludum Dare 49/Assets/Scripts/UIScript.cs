using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class UIScript : MonoBehaviour
{
    [SerializeField] GameObject timeLeftParent;
    [SerializeField] GameObject towerHeightParent;

    [SerializeField] TextMeshProUGUI timeLeftMinutesValue;
    [SerializeField] TextMeshProUGUI timeLeftTensValue;
    [SerializeField] TextMeshProUGUI timeLeftOnesValue;
    [SerializeField] TextMeshProUGUI timeLeftTenthsValue;
    [SerializeField] TextMeshProUGUI towerHeightValue;

    [SerializeField] EndScreenScript endScreenScript;

    public void SetTowerHeightValue(float value)
    {
        towerHeightValue.text = value.ToString("0.0");
    }

    public void SetTimeLeftValue(float timeLeft)
    {
        int minutes = Mathf.FloorToInt(timeLeft / 60f);
        int seconds = Mathf.FloorToInt(timeLeft - (minutes * 60f));
        int tens = Mathf.FloorToInt(seconds / 10f);
        int ones = seconds - (tens * 10);
        float spare = (timeLeft - minutes * 60f - seconds) * 10;

        if (spare >= 9.5f)
        {
            spare = 9.4f;
        }

        //Debug.Log($"spare = {spare}");

        string formattedText = "";

        if (timeLeft > 0f)
        {
            if (minutes > 0)
            {
                formattedText = $"{minutes.ToString()}:{seconds.ToString("00")}.{spare.ToString("0")}";
            }
            else if (seconds >= 10)
            {
                formattedText = $"{seconds.ToString("00")}.{spare.ToString("0")}";
            }
            else
            {
                formattedText = $"{seconds.ToString("0")}.{spare.ToString("0")}";
            }
        }
        else
        {
            formattedText = "0.0";
            timeLeftParent.SetActive(false);
            towerHeightParent.SetActive(false);

            GameObject playModeObject = GameObject.Find("PlayModeObject(Clone)");
            PlayModeScript playModeScript = playModeObject.GetComponent<PlayModeScript>();
            endScreenScript.ShowEndScreen(towerHeightValue.text, playModeScript.playMode);
        }

        //timeLeftValue.text = formattedText;

        timeLeftMinutesValue.text = minutes.ToString();
        timeLeftTensValue.text = tens.ToString();
        timeLeftOnesValue.text = ones.ToString();
        timeLeftTenthsValue.text = spare.ToString("0");
    }
}
