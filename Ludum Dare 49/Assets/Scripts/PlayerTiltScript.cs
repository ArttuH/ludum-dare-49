using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerTiltScript : MonoBehaviour
{
    [SerializeField] float maxTilt;
    [SerializeField] float tiltSpeed;
    [SerializeField] float referenceVelocity;
    [SerializeField] Rigidbody playerRigidbody;
    [SerializeField] PlayerScript playerScript;

    Vector2 previousVelocity;

    float tiltProgress;

    float tiltZero;

    private void Awake()
    {
        previousVelocity = Vector2.zero;
        tiltProgress = 0;
        tiltZero = transform.rotation.eulerAngles.x;
    }

    // Update is called once per frame
    void Update()
    {
        if (playerScript.moveInput.x < 0)
        {
            tiltProgress += tiltSpeed * Time.deltaTime;
            tiltProgress = Mathf.Min(1.0f, tiltProgress);
        }
        else if (playerScript.moveInput.x > 0)
        {
            tiltProgress -= tiltSpeed * Time.deltaTime;
            tiltProgress = Mathf.Max(-1.0f, tiltProgress);
        }
        else if (Mathf.Abs(tiltProgress) > tiltSpeed * Time.deltaTime)
        {
            tiltProgress += -1 * Mathf.Sign(tiltProgress) * tiltSpeed * Time.deltaTime; 
        }
        else
        {
            tiltProgress = 0f;
        }

        float targetRotation = tiltZero + tiltProgress * maxTilt;

        transform.rotation = Quaternion.Euler(transform.rotation.eulerAngles.x, transform.rotation.eulerAngles.y, targetRotation);

        //Vector2 velocity = playerRigidbody.velocity;

        //Vector3 rotation = transform.rotation.eulerAngles;

        //if (velocity.x > previousVelocity.x)
        //{
        //    if (rotation.x > -maxTilt)
        //    {
        //        transform.Rotate(Vector3.up, -tiltSpeed * Time.deltaTime);
        //    }
        //}
        //else if (velocity.x < previousVelocity.x)
        //{
        //    if (rotation.x < maxTilt)
        //    {
        //        transform.Rotate(Vector3.up, tiltSpeed * Time.deltaTime);
        //    }
        //}

        //previousVelocity = velocity;
    }
}
