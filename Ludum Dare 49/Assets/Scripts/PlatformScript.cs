using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlatformScript : MonoBehaviour
{
    [SerializeField] GameObject impactAudioPrefab;

    private void OnCollisionEnter(Collision collision)
    {
        Instantiate(impactAudioPrefab, transform.position, transform.rotation);
    }
}
