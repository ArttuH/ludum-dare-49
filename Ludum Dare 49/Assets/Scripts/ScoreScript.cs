using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScoreScript : MonoBehaviour
{
    public List<GameObject> boxes;

    [SerializeField] EndScreenScript endScreenScript;
    [SerializeField] UIScript uiScript;
    [SerializeField] Transform player;
    [SerializeField] Transform platform;

    [SerializeField] GameObject timeLeftParent;
    [SerializeField] GameObject towerHeightParent;

    [SerializeField] float timeLimit;
    [SerializeField] float timeRemaining;

    PlayModeScript playModeScript;

    float highestHeight;

    bool gameOver;

    private void Awake()
    {
        gameOver = false;

        boxes = new List<GameObject>();

        GameObject playModeObject = GameObject.Find("PlayModeObject(Clone)");
        playModeScript = playModeObject.GetComponent<PlayModeScript>();

        if (playModeScript.playMode == PlayMode.TimeTrial)
        {
            timeRemaining = timeLimit;
            timeLeftParent.SetActive(true);
            towerHeightParent.SetActive(true);
        }
        else
        {
            timeLeftParent.SetActive(false);
            towerHeightParent.SetActive(true);
            highestHeight = 0f;
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (!gameOver)
        {
            CalculateTowerHeight(playModeScript.playMode);

            if (playModeScript.playMode == PlayMode.TimeTrial)
            {
                UpdateTimeLeft();
            }
        }
    }

    public void AddBox(GameObject box)
    {
        boxes.Add(box);
    }

    void UpdateTimeLeft()
    {
        if (timeRemaining > 0)
        {
            timeRemaining -= Time.deltaTime;
            uiScript.SetTimeLeftValue(timeRemaining);
        }
    }

    public void CalculateTowerHeight(PlayMode playMode)
    {
        float platformHeight = platform.position.y + (platform.localScale.y * 0.5f);

        float maxHeight = 0;
        Vector3 highestPoint = new Vector3();

        foreach (GameObject box in boxes)
        {
            BoxScript boxScript = box.GetComponent<BoxScript>();

            /*if (!boxScript.isValid)
            {
                continue;
            }*/

            int cornerCountBelowPlatform = 0;

            Transform boxTransform = box.transform;

            // Top-left
            Vector3 topLeftPosition = (boxTransform.position +
                 0.5f * boxTransform.up * boxTransform.transform.localScale.y +
                 -0.5f * boxTransform.right * boxTransform.transform.localScale.x);

            float topLeftHeight = topLeftPosition.y;
            if (boxScript.isValid && topLeftHeight > maxHeight)
            {
                maxHeight = topLeftHeight;
                highestPoint = topLeftPosition;
            }
            if (topLeftHeight < platformHeight)
            {
                ++cornerCountBelowPlatform;
            }

            // Top-right
            Vector3 topRightPosition = (boxTransform.position +
                0.5f * boxTransform.up * boxTransform.transform.localScale.y +
                0.5f * boxTransform.right * boxTransform.transform.localScale.x);

            float topRightHeight = topRightPosition.y;
            if (boxScript.isValid && topRightHeight > maxHeight)
            {
                maxHeight = topRightHeight;
                highestPoint = topRightPosition;
            }
            if (topRightHeight < platformHeight)
            {
                ++cornerCountBelowPlatform;
            }

            // Bottom-right
            Vector3 bottomRightPosition = (boxTransform.position +
                 -0.5f * boxTransform.up * boxTransform.transform.localScale.y +
                 0.5f * boxTransform.right * boxTransform.transform.localScale.x);

            float bottomRightHeight = bottomRightPosition.y;
            if (boxScript.isValid && bottomRightHeight > maxHeight)
            {
                maxHeight = bottomRightHeight;
                highestPoint = bottomRightPosition;
            }
            if (bottomRightHeight < platformHeight)
            {
                ++cornerCountBelowPlatform;
            }

            // Bottom-left
            Vector3 bottomLeftPosition = (boxTransform.position +
                 -0.5f * boxTransform.up * boxTransform.transform.localScale.y +
                 -0.5f * boxTransform.right * boxTransform.transform.localScale.x);

            float bottomLeftHeight = bottomLeftPosition.y;
            if (boxScript.isValid && bottomLeftHeight > maxHeight)
            {
                maxHeight = bottomLeftHeight;
                highestPoint = bottomLeftPosition;
            }
            if (bottomLeftHeight < platformHeight)
            {
                ++cornerCountBelowPlatform;
            }

            if (playModeScript.playMode == PlayMode.Endurance)
            {
                if (boxScript.doNotDrop && cornerCountBelowPlatform == 4)
                {
                    gameOver = true;
                    endScreenScript.ShowEndScreen(highestHeight.ToString("0.0"), playModeScript.playMode);
            }
                else if (!boxScript.doNotDrop && cornerCountBelowPlatform == 0 && !boxScript.isGrabbed)
                {
                    boxScript.doNotDrop = true;
                }
            }
            
        }

        Debug.DrawLine(player.position, highestPoint);

        float normalizedHeight = Mathf.Max(0f, maxHeight - platformHeight);

        if (playMode == PlayMode.Endurance)
        {
            normalizedHeight = Mathf.Max(highestHeight, normalizedHeight);
            highestHeight = normalizedHeight;
        }

        uiScript.SetTowerHeightValue(normalizedHeight);
    }
}
