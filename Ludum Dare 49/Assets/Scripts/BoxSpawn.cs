using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoxSpawn : MonoBehaviour
{
    public Transform spawnPoint;

    public bool isFree;

    HashSet<Collider> intersectingColliders;

    private void Awake()
    {
        isFree = true;
        intersectingColliders = new HashSet<Collider>();
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider other)
    {
        intersectingColliders.Add(other);
        isFree = false;
    }

    private void OnTriggerExit(Collider other)
    {
        if (intersectingColliders.Contains(other))
        {
            intersectingColliders.Remove(other);
        }

        if (intersectingColliders.Count == 0)
        {
            isFree = true;
        }
    }
}
