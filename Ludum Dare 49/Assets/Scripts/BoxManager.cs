using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoxManager : MonoBehaviour
{
    [SerializeField] GameObject boxPrefab;

    [SerializeField] BoxSpawn[] boxSpawns;

    [SerializeField] ScoreScript scoreScript;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        SpawnBoxes();
    }

    void SpawnBoxes()
    {
        foreach (BoxSpawn boxSpawn in boxSpawns)
        {
            if (boxSpawn.isFree)
            {
                boxSpawn.isFree = false;

                float scale = Random.Range(1.0f, 2.0f);
                GameObject box = Instantiate(boxPrefab, boxSpawn.spawnPoint.position + Vector3.up * scale * 0.5f, Quaternion.identity);
                box.transform.localScale = new Vector3(scale, scale, 1.0f);
                Rigidbody boxRigidbody = box.GetComponent<Rigidbody>();
                boxRigidbody.mass *= scale;
                box.transform.parent = transform;
                BoxScript boxScript = box.GetComponent<BoxScript>();
                boxScript.Initialize();

                scoreScript.AddBox(box);
            }
        }
    }
}
